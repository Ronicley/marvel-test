import './App.css';
import './index.css';
import {Provider} from "react-redux";
import {BrowserRouter as Router} from 'react-router-dom'
import PropTypes from 'prop-types'
import Routes from './pages'

function App({store}) {
  return (
    <Provider store={store}>
      <Router>
        <Routes/>
      </Router>
    </Provider>
  );
}

App.propTypes = {
  store: PropTypes.object.isRequired
}

export default App;
