import { configureStore } from '@reduxjs/toolkit';
import characterReducer from '../pages/characters/characterSlice';
import navbarReducer from '../components/navbar/navbarSlice'

export default configureStore({
  reducer: {
    character: characterReducer,
    navbar: navbarReducer,
  },
});