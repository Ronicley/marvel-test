import React from 'react'
import PropTypes from 'prop-types'
import Navbar from '../components/navbar'
import Sidebar from '../components/sidebar'
import makeStyles from '@material-ui/core/styles/makeStyles'
import CssBaseline from '@material-ui/core/CssBaseline'
import Toolbar from '@material-ui/core/Toolbar'

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex'
  },
  content: {
    position: 'relative',
    flexGrow: 1,
    padding: theme.spacing(3)
  }
}))

const CoreLayout = ({children}) => {
  const classes = useStyles()
  return (
    <div className={classes.root}>
      <CssBaseline/>
      <Navbar/>
      <Sidebar/>
      <div className={classes.content}>
        <Toolbar/>
        {children}
      </div>
    </div>
  )
}

CoreLayout.propTypes = {
  children: PropTypes.element.isRequired
}

export default CoreLayout
