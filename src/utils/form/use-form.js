import { useCallback, useState, useMemo } from 'react';
import _ from 'lodash';
import validate from 'validate.js';

export function useForm(initialState, schema, onSubmit) {
  const [form, setForm] = useState(initialState);
  const [validator, setValidator] = useState({
    isValid: false,
    touched: {},
    errors: {}
  });

  useMemo(() => {
    setValidator((prevValidator) => ({
      ...prevValidator,
      isValid: !validate(form, schema)
    }))
  }, [setValidator, form, schema]);

  const handleChange = useCallback((event) => {
    event.persist();

    let upForm = _.setWith(_.clone({ ...form }), event.target.name, event.target.type === 'checkbox'? event.target.checked: event.target.value, _.clone)

    setForm(upForm);

    const errors = validate(upForm, schema);

    let upValidator = _.setWith(_.clone({ ...validator }), `touched.${event.target.name}`, true, _.clone);

    upValidator.errors = errors;
    setValidator(upValidator);
  }, [validator, schema, form]);

  const hasError = (field) => (!!(_.has(validator.touched, `${field}`) && _.has(validator.errors, `${field}`)));

  const getError = (field) => {
    if (hasError(field)) {
      return _.get(validator.errors, `${field}`)[0];
    }

    return null;
  };

  const isValid = () => {
    return !validator.isValid
  };

  const resetForm = useCallback(() => {
    console.log(initialState)
    if (!_.isEqual(initialState, form)) {
      setForm(initialState);
    }
  }, [form, initialState]);

  const setInForm = useCallback((name, value) => {

    setForm(form => _.setWith(_.clone(form), name, value, _.clone));
  }, []);

  const handleSubmit = useCallback((event) => {
    if (event) {
      event.preventDefault();
    }
    if (onSubmit) {
      onSubmit();
    }
  }, [onSubmit]);

  return {
    form,
    handleChange,
    handleSubmit,
    resetForm,
    setForm,
    setInForm,
    hasError,
    getError,
    isValid,
  }
}
