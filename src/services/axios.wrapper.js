import axios from "axios";
import apiUrl from './api'

const instance = axios.create({
  baseURL: apiUrl,
  timeout: 4000,
  headers: {
    'Content-Type': 'application/json'
  },
  params: {
    ts: '1616191205825',
    apikey: 'df44caf3da63b91444752d1247a5940e',
    hash: 'fd7f3ee154a96be147e857513097f85e'
  },
});

export default instance