import React, {useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import {Button, CardActions, IconButton} from "@material-ui/core";
import PropTypes from 'prop-types'
import styles from './card.styles'
import clsx from "clsx";
import {Edit} from "@material-ui/icons";

const useStyles = makeStyles(styles);

export default function CharacterCard({item, onClick, onEdit}) {
  const [character, setCharacter] = useState()
  const classes = useStyles();

  useEffect(() => {
    setCharacter(item)
  }, [item])

  return (
    <div>
      <Card className={classes.root}>
        <CardMedia
          className={classes.media}
        >
          {
            character?.file?(
              // eslint-disable-next-line jsx-a11y/img-redundant-alt
              <img src={character.file} alt={"Marvel hero picture"}/>
            ):(
              // eslint-disable-next-line jsx-a11y/img-redundant-alt
              <img src={`${character?.thumbnail?.path}.${character?.thumbnail?.extension}`} alt={"Marvel hero picture"}/>
            )
          }

        </CardMedia>
        <CardContent>
          <div className={classes.cardTitle}>
            <Typography gutterBottom variant="h5" component="h2">
              {character?.name}
            </Typography>
          </div>
        </CardContent>
        <CardActions className="flex justify-end" disableSpacing>
          <Button onClick={onClick} variant="text" size="small" color="inherit" className={clsx(classes.button)}>
            <Typography className="normal-case text-gray-500" variant="body2">
              Ver Detalhes
            </Typography>
          </Button>
          <IconButton onClick={onEdit} size="small" aria-label="edit">
            <Edit fontSize="inherit"/>
          </IconButton>
        </CardActions>
      </Card>
    </div>
  );
}

CharacterCard.propTypes = {
  onClick: PropTypes.func,
  onEdit: PropTypes.func
}