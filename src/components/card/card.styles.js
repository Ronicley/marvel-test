// eslint-disable-next-line import/no-anonymous-default-export
export default (theme) => ({
  root: {
    width: 345,
    maxWidth: 345,
  },
  media: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  cardTitle: {
    background: 'linear-gradient(0deg, rgba(255,255,255,1) 0%, rgba(255,255,255,1) 35%, rgba(0,212,255,0) 81%) !important'
  },
  button: {
    margin: theme.spacing(1),
  },
})