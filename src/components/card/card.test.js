import {render, screen} from '@testing-library/react';
import CharacterCard from './index';

test('renders learn react link', () => {
  render(
    <CharacterCard
      item={{
        name: "3-D Man",
        thumbnail: {
          extension: "jpg",
          path: "http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784"
        }
      }}
    />);
  const linkElement = screen.getByText(/Ver Detalhes/i);
  expect(linkElement).toBeInTheDocument();
});

