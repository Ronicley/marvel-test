import {createSlice} from '@reduxjs/toolkit';

export const navbarSlice = createSlice({
  name: 'navbar',
  initialState: {
    searchText: '',
    cleanSearchText: false,
  },
  reducers: {
    searchCharacters: (state, action) => {
      state.searchText += action.payload
    },
    cleanSearch: (state, action) => {
      state.cleanSearchText = action.payload
    },
  },
});

export const {searchCharacters, cleanSearch} = navbarSlice.actions;

export const selectSearchText = state => state.navbar.searchText;
export const cleanSearchText = state => state.navbar.cleanSearchText;

export default navbarSlice.reducer;