import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Drawer from '@material-ui/core/Drawer'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Toolbar from '@material-ui/core/Toolbar'
import {Person} from '@material-ui/icons'
import { NavLink } from 'react-router-dom'
import {
  CHARACTERS_PATH
} from '../../constants/paths'
import styles from "./sidebar.styles";

const useStyles = makeStyles(styles)

const Sidebar = () => {
  const classes = useStyles()
  const sideBarLinks = [
    {
      link: CHARACTERS_PATH,
      iconComponent: <Person />,
      sideBarName: 'Personagens'
    },
  ]
  return (
    <Drawer
      className={classes.drawer}
      variant="permanent"
      classes={{
        paper: classes.drawerPaper
      }}>
      <Toolbar />
      <div className={classes.drawerContainer}>
        <List>
          {sideBarLinks.map((item, index) => {
            return (
              <ListItem
                key={index}
                component={NavLink}
                to={item.link}
                activeStyle={{
                  backgroundColor: '#b8b8b8'
                }}>
                <ListItemIcon>{item.iconComponent}</ListItemIcon>
                {item.sideBarName}
                <ListItemText />
              </ListItem>
            )
          })}
        </List>
      </div>
    </Drawer>
  )
}

export default Sidebar
