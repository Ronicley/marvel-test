const drawerWidth = 240
// eslint-disable-next-line import/no-anonymous-default-export
export default () => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0
  },
  drawerPaper: {
    width: drawerWidth
  },
  drawerContainer: {
    overflow: 'auto'
  }
})
