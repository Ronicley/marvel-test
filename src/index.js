import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import store from './store';

ReactDOM.render(
  // para remover o warning de strict mode do component do material ui backdrop
  // Fonte: https://stackoverflow.com/questions/61220424/material-ui-drawer-finddomnode-is-deprecated-in-strictmode
  // <React.StrictMode>
  <App store={store}/>,
  // </React.StrictMode>,
  document.getElementById('root')
);

reportWebVitals();
