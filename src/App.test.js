import { render, screen } from '@testing-library/react';
import App from './App';
import store from './store'

test('renders principal page', () => {
  render(<App  store={store}/>);
  const linkElement = screen.getByText(/Algo deu errado! 404!/i);
  expect(linkElement).toBeInTheDocument();
});
