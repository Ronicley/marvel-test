export const CHARACTERS_PATH = '/characters'
export const CHARACTERS_DETAILS_PATH = '/characters-details/:id'
export const CHARACTERS_EDIT_PATH = '/characters-edit/:id'
