import { createSlice } from '@reduxjs/toolkit';

export const characterSlice = createSlice({
  name: 'character',
  initialState: {
    itemEdited: {},
    id: ''
  },
  reducers: {
    setItemEdited: (state, action) => {
      state.itemEdited = action.payload;
    },
    setCharacterId: (state, action)=> {
      state.id = action.payload;
    }
  },
});

export const { setItemEdited, setCharacterId } = characterSlice.actions;

export const selectItemEdited = state => state.character.itemEdited;
export const selectCharacterId = state => state.character.id;

export default characterSlice.reducer;