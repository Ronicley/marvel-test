import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import CharacterCard from "../../components/card";
import {Backdrop, CircularProgress} from "@material-ui/core";
import {getAllCharacters, getAllCharactersByName} from './character.service';
import {useHistory} from 'react-router-dom';
import {CHARACTERS_DETAILS_PATH, CHARACTERS_EDIT_PATH} from "../../constants/paths";
import makeStyles from "@material-ui/core/styles/makeStyles";
import styles from "../charactersEdit/CharactersEdit.styles";
import {selectSearchText, cleanSearchText, cleanSearch} from '../../components/navbar/navbarSlice';
import {selectCharacterId, selectItemEdited} from './characterSlice'

const useStyles = makeStyles(styles)

export default function Character() {
  const [results, setResults] = useState([])
  const [loading, setLoading] = useState(false)
  const searchText = useSelector(selectSearchText);
  const itemEdited = useSelector(selectItemEdited);
  const characterId = useSelector(selectCharacterId);
  const clean = useSelector(cleanSearchText);
  const dispatch = useDispatch();
  let history = useHistory();
  const classes = useStyles();

  const handleGetAllCharacters = async () => {
    setLoading(true)
    try {
      let {results} = await getAllCharacters();
      setResults(results);
    } catch (e) {
      console.error(e);
    }
    setLoading(false);
  }

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const handleGetCharactersByName = async () => {
    setLoading(true);
    try {
      let data = await getAllCharactersByName(searchText);
      setResults(data?.results);
    } catch (e) {
      console.error(e);
    }
    setLoading(false);
  }

  const handleGetCharactersDetails = (id) => {
    history.push(`${CHARACTERS_DETAILS_PATH.replaceAll('/:id', '')}/${id}`);
  }

  const handleEdit = (id) => {
    history.push(`${CHARACTERS_EDIT_PATH.replaceAll('/:id', '')}/${id}`);
  }

  useEffect(() => {
    (
      async () => {
        await handleGetAllCharacters();
      }
    )()
  }, [])

  useEffect(() => {
    return () => {
      (
        async () => {
          await handleGetCharactersByName();
        }
      )()
    }
  }, [searchText])

  useEffect(() => {
    if (clean) {
      (
        async () => {
          await handleGetAllCharacters();
        }
      )()
      dispatch(cleanSearch(false));
    }
  }, [dispatch, clean])

  useEffect(() => {
    if (itemEdited && results) {
      let char, index, resultsTemp

      resultsTemp = results
      char = results?.find((element) => element.id === parseInt(characterId))
      index = results?.findIndex((element) => element.id === parseInt(characterId))

      char = {
        ...char,
        ...itemEdited
      }
      resultsTemp[index] = char
      setResults(resultsTemp)
    }
  }, [itemEdited, characterId, results])

  return (
    <div className="flex flex-wrap justify-around items-start">
      {
        loading ? (
          <Backdrop className={classes.backdrop} open={loading}>
            <CircularProgress color="inherit"/>
          </Backdrop>
        ) : (
          results?.map((element, index) => {
            return (
              <div key={index} className="my-4">
                <CharacterCard
                  onClick={() => handleGetCharactersDetails(element.id)}
                  onEdit={() => handleEdit(element.id)}
                  item={element}/>
              </div>
            )
          })
        )
      }
    </div>
  );
}