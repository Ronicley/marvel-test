import axios from '../../services/axios.wrapper';


export const getAllCharacters = async () => {

  try {
    let {data: {data}} = await axios.get('/characters')
    return data
  } catch (e) {
    console.error(e)
  }
}

export const getAllCharactersByName = async (name) => {
  try {
    if (name) {
      let {data: {data}} = await axios.get(`/characters?nameStartsWith=${name}`)
      return data
    }

  } catch (e) {
    console.error(e)
  }
}