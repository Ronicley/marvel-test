import {CHARACTERS_PATH as path} from '../../constants/paths'
import Character from "./Characters";
// eslint-disable-next-line import/no-anonymous-default-export
export default {
  path,
  component: <Character/>
}
