import React from 'react'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'
import styles from './notfoundPage.styles'
import clsx from "clsx";

const useStyles = makeStyles(styles)

function NotFoundPage() {
  const classes = useStyles()

  return (
    <div className={clsx(classes.root, "w-full h-full")}>
      <Typography variant="h2">Algo deu errado! 404!</Typography>
      <p>Essa página não está disponível</p>
    </div>
  )
}

export default NotFoundPage
