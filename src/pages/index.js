import React from 'react'
import {Switch, Route} from 'react-router-dom'
import NotFoundPage from './notfound'
import character from './characters'
import charactersDetails from "./charactersDetails";
import characterEdit from './charactersEdit'
import CoreLayout from "../layout";

export default function createRoutes() {
  return (
    <CoreLayout>
      <Switch>
        <Route path={character.path}>
          {/* eslint-disable-next-line react/jsx-pascal-case */}
          {character.component}
        </Route>

        <Route path={charactersDetails.path}>
          {/* eslint-disable-next-line react/jsx-pascal-case */}
          {charactersDetails.component}
        </Route>

        <Route path={characterEdit.path}>
          {/* eslint-disable-next-line react/jsx-pascal-case */}
          {characterEdit.component}
        </Route>

        <Route component={NotFoundPage} />
      </Switch>
    </CoreLayout>
  )
}