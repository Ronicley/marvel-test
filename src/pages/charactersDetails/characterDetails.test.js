import React from 'react'
import {render, screen, fireEvent, waitFor} from '@testing-library/react'
import {Router} from 'react-router-dom'
import store from '../../store';
import {Provider} from "react-redux";
import Routes from "../index";
import {createMemoryHistory} from "history";


test('Page load', async () => {
  const history = createMemoryHistory();
  const route = '/characters-details/1011334"';
  history.push(route);

  const element = render(
    <Provider store={store}>
      <Router history={history}>
        <Routes/>
      </Router>
    </Provider>
  )

  await waitFor(() => expect(screen.getByText(/Voltar/i)).toBeInTheDocument(),
    {
      container: element.component
    }
  )
})