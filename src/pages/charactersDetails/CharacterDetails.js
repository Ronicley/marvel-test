import React, {useEffect, useState} from 'react'
import styles from './CharacterDetails.styles'
import makeStyles from "@material-ui/core/styles/makeStyles";
import {useHistory} from "react-router-dom";
import {CHARACTERS_PATH} from "../../constants/paths";
import {
  Backdrop, Button,
  Card,
  CardHeader,
  CircularProgress,
} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import {getAllCharactersDetailsById} from './characterDetails.service'
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import {useParams} from "react-router";
import {ArrowBack} from "@material-ui/icons";

const useStyles = makeStyles(styles);

const CharacterDetails = () => {
  const {id} = useParams();
  let history = useHistory();
  const [characterDetails, setCharacterDetails] = useState([]);
  const [loading, setLoading] = useState(false);

  const handleGetCharactersDetails = async (id) => {
    setLoading(true)
    try {
      let {results} = await getAllCharactersDetailsById(id)
      setCharacterDetails(results)
    } catch (e) {
      history.push('/')
      console.error(e)
    }
    setLoading(false)
  }

  useEffect(() => {
    if (id) {
      (
        async () => {
          await handleGetCharactersDetails(id)
        }
      )()
    } else {
      history.push(CHARACTERS_PATH);
    }
  }, [id, history])

  const classes = useStyles();

  return (
    <div>
      <div className="my-4">
        <Button
          onClick={() => {
          history.push(CHARACTERS_PATH)
        }}>
          <ArrowBack/>
          Voltar
        </Button>
      </div>
      {
        loading ? (
          <Backdrop className={classes.backdrop} open={loading}>
            <CircularProgress color="inherit"/>
          </Backdrop>
        ) : (
          <div>
            {
              characterDetails.map((element, index) => (
                <Card key={index} className={classes.cardRoot}>
                  <CardHeader
                    title={element.title}
                    subheader={`De ${element.startYear} até ${element.endYear}`}
                  />
                  <CardMedia
                    className={classes.media}
                  >
                    {/* eslint-disable-next-line jsx-a11y/img-redundant-alt */}
                    <img src={`${element?.thumbnail?.path}.${element?.thumbnail?.extension}`} alt={"Serie picture"}/>
                  </CardMedia>
                  <CardContent>
                    <Typography variant="body2" color="textSecondary" component="p">
                      {element.description ? element.description : "Esta serie não possui uma descrição"}
                    </Typography>
                  </CardContent>
                </Card>
              ))
            }
          </div>
        )
      }
    </div>
  )
}

export default CharacterDetails