import axios from '../../services/axios.wrapper';

export const getAllCharactersDetailsById = async (characterId) => {

  try {
    let {data: {data}} = await axios.get(`/characters/${characterId}/series`)
    return data
  } catch (e) {
    console.error(e)
  }
}

