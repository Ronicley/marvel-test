import {CHARACTERS_DETAILS_PATH as path} from '../../constants/paths'
import CharacterDetails from "./CharacterDetails";
// eslint-disable-next-line import/no-anonymous-default-export
export default {
  path,
  component: <CharacterDetails/>
}
