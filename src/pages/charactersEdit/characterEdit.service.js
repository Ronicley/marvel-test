import axios from '../../services/axios.wrapper';

export const getAllCharacterById = async (characterId) => {

  try {
    let {data: {data}} = await axios.get(`/characters/${characterId}`)
    return data
  } catch (e) {
    console.error(e)
  }
}
