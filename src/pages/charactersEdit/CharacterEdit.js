import React, {useEffect, useState} from 'react'
import {useHistory, useParams} from "react-router";
import {CHARACTERS_PATH} from "../../constants/paths";
import {getAllCharacterById} from "./characterEdit.service";
import {Backdrop, Button, CircularProgress, TextField} from "@material-ui/core";
import styles from './CharactersEdit.styles'
import makeStyles from "@material-ui/core/styles/makeStyles";
import {useForm} from "../../utils/form";
import {entitySchema} from "./validatorSchema";
import Typography from "@material-ui/core/Typography";
import {setItemEdited, setCharacterId} from '../characters/characterSlice'
import SaveIcon from '@material-ui/icons/Save';
import {useDispatch} from "react-redux";

const useStyles = makeStyles(styles)

const CharacterEdit = () => {
  const {id} = useParams();
  const dispath = useDispatch();
  let history = useHistory();
  const classes = useStyles();
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState();

  const {
    form,
    handleChange,
    setForm,
    setInForm,
    hasError,
    getError,
    isValid
  } = useForm(data, entitySchema);

  const handleSubmit = () => {
    if (form !== null && (form.id === null || form.id === undefined)) {
      dispath(setItemEdited(form));
      dispath(setCharacterId(id));
      history.push(CHARACTERS_PATH);
    }
  };

  const onRemove = () => {
    setInForm('file', '');
  }

  useEffect(() => {
    if (id) {
      (
        async () => {
          setLoading(true);

          try {
            let {results} = await getAllCharacterById(id);
            results[0].file = `${results[0]?.thumbnail?.path}.${results[0]?.thumbnail?.extension}`;
            delete results[0].thumbnail;
            setData({name: results[0].name, file: results[0].file});
          } catch (e) {
            history.push('/');
            console.error(e);
          }

          setLoading(false);
        }
      )()
    } else {
      history.push(CHARACTERS_PATH);
    }
  }, [id, history])

  useEffect(() => {
    if (
      (data && !form) ||
      (data && form && data.id !== form.id)
    ) {
      setForm(data);
    }
  }, [form, data, setForm]);

  return (
    <div>
      {
        loading ? (
          <Backdrop className={classes.backdrop} open={loading}>
            <CircularProgress color="inherit"/>
          </Backdrop>
        ) : (
          <form onSubmit={handleSubmit} className="flex flex-wrap h-1/2 flex-col  w-1/2" noValidate autoComplete="off">
            <TextField
              className="mb-24"
              type="text"
              label="Nome"
              autoFocus
              id="name"
              name="name"
              value={form?.name || ''}
              onChange={handleChange}
              validations={{
                isExisty: true,
              }}
              variant="outlined"
              required
              fullWidth
              error={hasError('name')}
              helperText={getError('name')}
              autoComplete="off"
            />
            <div className="my-10">

              <TextField
                className="mb-24"
                type="text"
                label="URL da imagem"
                autoFocus
                id="file"
                name="file"
                value={form?.file || ''}
                onChange={handleChange}
                validations={{
                  isExisty: true,
                }}
                variant="outlined"
                required
                fullWidth
                error={hasError('file')}
                helperText={getError('file')}
                autoComplete="off"
              />

              {
                form?.file && (
                  <div className='flex flex-col justify-around items-end mt-10'>
                    <img className="mb-3" src={form?.file} alt="File"/>
                    <Button onClick={onRemove} variant="outlined" size="small" color="inherit">
                      <Typography className="normal-case text-gray-700" variant="body2">
                        Remover foto
                      </Typography>
                    </Button>
                  </div>
                )
              }

            </div>
            <Button
              variant="contained"
              className="ml-8 main-btn text-white bg-gray-800 hover:bg-gray-700"
              disabled={isValid()}
              onClick={handleSubmit}
            >
              <SaveIcon/>
            </Button>
          </form>

        )
      }
    </div>
  )
}

export default CharacterEdit