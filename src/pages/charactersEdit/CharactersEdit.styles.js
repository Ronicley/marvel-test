// eslint-disable-next-line import/no-anonymous-default-export
export default (theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
})