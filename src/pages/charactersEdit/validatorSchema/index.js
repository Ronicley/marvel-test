export const entitySchema = {
  name: {
    presence: { allowEmpty: false, message: '^Este campo é obrigatório' },
    length: {
      minimum: 1,
      tooShort: "^Por favor digite pelo menos 1 caractere",
    },
  },
  file: {
    presence: { allowEmpty: false, message: '^Este campo é obrigatório' },
    length: {
      minimum: 1,
      tooShort: "^Por favor digite pelo menos 1 caractere",
    },
  },
};
