import {CHARACTERS_EDIT_PATH as path} from '../../constants/paths'
import CharacterEdit from "./CharacterEdit";
// eslint-disable-next-line import/no-anonymous-default-export
export default {
  path,
  component: <CharacterEdit/>
}
